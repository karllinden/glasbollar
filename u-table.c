/*
 * u-table.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "u-table.h"
#include "print-error.h"

#define U_TABLE_K_MAX_DEFAULT 2
#define U_TABLE_I_MAX_DEFAULT 1

static bool u_table_has_atexit = false;

static num_t * u_table = NULL;
static num_t u_table_k_max = U_TABLE_K_MAX_DEFAULT;
static num_t u_table_i_max = U_TABLE_I_MAX_DEFAULT;

static inline num_t *
u_pointer(const num_t k, const num_t i)
{
    assert(k > U_TABLE_K_MAX_DEFAULT);
    assert(k <= u_table_k_max);
    assert(i > U_TABLE_I_MAX_DEFAULT);
    assert(i <= u_table_i_max);
    assert(u_table != NULL);
    return u_table +
           (k-U_TABLE_K_MAX_DEFAULT-1) *
           (u_table_i_max-U_TABLE_I_MAX_DEFAULT) +
           (i-U_TABLE_I_MAX_DEFAULT-1);
}

int
u_table_alloc(num_t k, num_t i)
{
    if (k < u_table_k_max) {
        k = u_table_k_max;
    }
    if (i < u_table_i_max) {
        i = u_table_i_max;
    }
    if (k == u_table_k_max && i == u_table_i_max) {
        return 0;
    }

    if (!u_table_has_atexit) {
        u_table_has_atexit = true;
        if (atexit(&u_table_free)) {
            print_error_errno("could not set atexit function");
            u_table_has_atexit = false;
        }
    }

    const size_t size = (k-U_TABLE_K_MAX_DEFAULT) *
                        (i-U_TABLE_I_MAX_DEFAULT) *
                        sizeof(num_t);
    num_t * const new = malloc(size);
    if (new == NULL) {
        print_error_errno("could not allocate memory");
        return 1;
    }
    memset(new, 0, size);

    if (u_table != NULL && u_table_i_max != U_TABLE_I_MAX_DEFAULT) {
        for (num_t j = U_TABLE_K_MAX_DEFAULT + 1;
             j <= u_table_k_max;
             ++j)
        {
            num_t * const src = u_pointer(j, U_TABLE_I_MAX_DEFAULT + 1);
            num_t * const dest = new + (j-U_TABLE_K_MAX_DEFAULT-1) *
                                       (i-U_TABLE_I_MAX_DEFAULT);
            memcpy(dest, src,
                   (u_table_i_max-U_TABLE_I_MAX_DEFAULT)*sizeof(num_t));
        }
        free(u_table);
    }

    u_table = new;
    u_table_k_max = k;
    u_table_i_max = i;

    return 0;
}

void
u_table_free(void)
{
    free(u_table);
    u_table = NULL;
    u_table_k_max = U_TABLE_K_MAX_DEFAULT;
    u_table_i_max = U_TABLE_I_MAX_DEFAULT;
    return;
}

static inline void
u_table_size(const num_t k, const num_t i)
{
    assert(k > U_TABLE_K_MAX_DEFAULT);
    assert(i > U_TABLE_I_MAX_DEFAULT);

    num_t newk = u_table_k_max;
    num_t newi = u_table_i_max;

    if (k > u_table_k_max) {
        newk = k;
    }
    if (i > u_table_i_max) {
        newi = i;
    }
    if (newk != u_table_k_max || newi != u_table_i_max) {
        if (u_table_alloc(newk, newi)) {
            abort();
        }
    }
    return;
}

num_t
u_table_calc(const num_t k, const num_t i)
{
    assert(k != 0);

    if (i == 0) {
        return 0;
    } else if (i == 1) {
        return 1;
    } else if (k == 1) {
        return i;
    } else if (k == 2) {
        /* written to avoid integer overflow */
        num_t fac1 = i;
        num_t fac2 = i + 1;
        if (i % 2 == 0) {
            fac1 >>= 1;
        } else {
            fac2 >>= 1;
        }
        return fac1 * fac2;
    }

    u_table_size(k, i);

    num_t * const up = u_pointer(k, i);
    if (*up == 0) {
        *up = u_table_calc(k,i-1) + u_table_calc(k-1,i-1) + 1;
    }

    return *up;
}
