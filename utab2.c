/*
 * utab2.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

#include "print-error.h"
#include "u-table.h"

#define K_MAX_DEFAULT  15
#define I_MAX_DEFAULT  20

static char * caption = NULL;
static char * label = NULL;

int __attribute__((nonnull))
main(int argc, char ** const argv)
{
    int exit_status = EXIT_SUCCESS;

    FILE * file = stdout;

    char * filename = NULL;

    num_t k_max = K_MAX_DEFAULT;
    num_t i_max = I_MAX_DEFAULT;

    const gop_option_t options[] = {
        {"caption", '\0', GOP_STRING, &caption, NULL, "caption",
            "CAPTION"},
        {"output", 'o', GOP_STRING, &filename, NULL,
            "output filename", "FILENAME"},
        {"k-max", '\0', GOP_NUM_T, &k_max, NULL, "max k-value",
            "K_MAX"},
        {"label", '\0', GOP_STRING, &label, NULL, "label", "LABEL"},
        {"i-max", '\0', GOP_NUM_T, &i_max, NULL, "max i-value",
            "I_MAX"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (filename != NULL && filename[0] != '\0') {
        file = fopen(filename, "w");
        if (file == NULL) {
            print_error_errno("could not open %s for writing",
                              filename);
            goto error;
        }
    }

    if (u_table_alloc(k_max, i_max)) {
        goto error;
    }

    fputs("\\begin{table}[H]\n"
          "\\centering\n"
          "\\footnotesize\n", file);
    if (caption != NULL) {
        fprintf(file, "\\caption[%s]{%s}\n", caption, caption);
    }
    if (label != NULL) {
        fprintf(file, "\\label{%s}\n", label);
    }

    fputs("\\begin{tabular}{", file);
    fputs("r|", file);
    for (num_t k = 1; k <= k_max; ++k) {
        fputc('r', file);
    }
    fputs("}\n", file);

    fputs("\\\\\n", file);

    fputs("$i$ ", file);
    for (num_t k = 1; k <= k_max; ++k) {
        fprintf(file, "& $u_{%"PRINUM",i}$ ", k);
    }
    fputs("\\\\\n", file);

    fputs("\\hline\n", file);

    for (num_t i = 0; i <= i_max; ++i) {
        fprintf(file, "%"PRINUM" ", i);
        for (num_t k = 1; k <= k_max; ++k) {
            fprintf(file, "& %" PRINUM " ", u_table_calc(k, i));
        }
        fputs("\\\\\n", file);
    }

    fputs("\\end{tabular}\n"
          "\\end{table}\n", file);

    if (false) {
    error:
        exit_status = EXIT_FAILURE;
    }

    if (file != NULL && file != stdout) {
        if (fclose(file)) {
            print_error_errno("could not close file");
        }
    }

    return exit_status;
}
