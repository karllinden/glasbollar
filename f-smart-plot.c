/*
 * fplot.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

#include "f-table.h"
#include "num.h"
#include "print-error.h"

int __attribute__((nonnull))
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;

    FILE * file1 = NULL;
    FILE * file2 = NULL;

    char * filename1 = NULL;
    char * filename2 = NULL;

    num_t k = 0;
    num_t m = 0;

    const gop_option_t options[] = {
        {"file1", '1', GOP_STRING, &filename1, NULL,
            "output 1 filename", "FILENAME"},
        {"file2", '2', GOP_STRING, &filename2, NULL,
            "output 2 filename", "FILENAME"},
        {NULL, 'k', GOP_NUM_T, &k, NULL, "k-value", "K"},
        {NULL, 'm', GOP_NUM_T, &m, NULL, "m-value", "M"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (k == 0 || m == 0) {
        print_error("specify -k and -m (to positive values)");
        goto error;
    }

    if (filename1 == NULL || filename2 == NULL) {
        print_error("specify both --file1|-1 and --file2|-2");
        goto error;
    }

    if (f_table_alloc(k, m-1)) {
        goto error;
    }

    file1 = fopen(filename1, "w");
    if (file1 == NULL) {
        print_error_errno("could not open %s for writing", filename1);
        goto error;
    }

    file2 = fopen(filename2, "w");
    if (file2 == NULL) {
        print_error_errno("could not open %s for writing", filename2);
        goto error;
    }

    for (num_t n = 1; n <= m; ++n) {
        fprintf(file1, "%" PRINUM " %" PRINUM "\n", n,
                f_table_calc(k-1, n-1));
        fprintf(file2, "%" PRINUM " %" PRINUM "\n", n,
                f_table_calc(k, m-n));
    }

    exit_status = EXIT_SUCCESS;

error:
    if (file1 != NULL && fclose(file1)) {
        print_error_errno("could not close %s", filename1);
    }
    if (file2 != NULL && fclose(file2)) {
        print_error_errno("could not close %s", filename2);
    }

    return exit_status;
}
