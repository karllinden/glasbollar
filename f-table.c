/*
 * f-table.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "f-table.h"
#include "num.h"
#include "print-error.h"

static bool f_table_do_huge = false;
static bool f_table_has_atexit = false;

static num_t * f_table = NULL;
static num_t f_table_k_max = 1;
static num_t f_table_m_max = 0;

void
f_table_huge(void)
{
    f_table_do_huge = true;
    return;
}

static inline num_t *
f_pointer(const num_t k, const num_t m)
{
    assert(k > 1);
    assert(k <= f_table_k_max);
    assert(m > 0);
    assert(m <= f_table_m_max);
    assert(f_table != NULL);
    return f_table + (k-2) * f_table_m_max + (m-1);
}

int
f_table_alloc(num_t k, num_t m)
{
    if (k < f_table_k_max) {
        k = f_table_k_max;
    }
    if (m < f_table_m_max) {
        m = f_table_m_max;
    }
    if (k == f_table_k_max && m == f_table_m_max) {
        return 0;
    }

    if (f_table_do_huge && m > f_table_m_max) {
        num_t newm = f_table_m_max;
        do {
            newm <<= 2;
        } while (newm < m);
        m = newm;
    }

    if (!f_table_has_atexit) {
        f_table_has_atexit = true;
        if (atexit(&f_table_free)) {
            print_error_errno("could not set atexit function");
            f_table_has_atexit = false;
        }
    }

    const size_t size = (k-1) * m * sizeof(num_t);
    num_t * const new = malloc(size);
    if (new == NULL) {
        print_error_errno("could not allocate memory");
        return 1;
    }
    memset(new, 0, size);

    if (f_table != NULL) {
        for (num_t j = 2; j <= f_table_k_max; ++j) {
            num_t * const src = f_pointer(j, 1);
            num_t * const dest = new + (k-2) * m;
            memcpy(dest, src, f_table_m_max * sizeof(num_t));
        }
        free(f_table);
    }

    f_table = new;
    f_table_k_max = k;
    f_table_m_max = m;

    return 0;
}

void
f_table_free(void)
{
    free(f_table);
    f_table = NULL;
    f_table_k_max = 1;
    f_table_m_max = 0;
    return;
}

static inline void
f_table_size(const num_t k, const num_t m)
{
    assert(k > 1);
    assert(m > 0);

    num_t newk = f_table_k_max;
    num_t newm = f_table_m_max;

    if (k > f_table_k_max) {
        newk = k;
    }
    if (m > f_table_m_max) {
        newm = m;
    }
    if (newk != f_table_k_max || newm != f_table_m_max) {
        if (f_table_alloc(newk, newm)) {
            abort();
        }
    }
    return;
}

num_t
f_table_calc(const num_t k, const num_t m)
{
    assert(k != 0);

    if (k == 1) {
        return m;
    } else if (m == 0) {
        return 0;
    }

    f_table_size(k, m);

    num_t * const fp = f_pointer(k, m);
    if (*fp == 0) {
        /* The first iteration (n=1) has been moved outside the for
         * loop. Since f(k-1,1-1) = f(k-1,0) = 0 it must be less than or
         * equal to f(k,m-1). Thus the maximum value can be set to
         * f(k, m-1) at first. */
        num_t nu_min = f_table_calc(k,m-1);
#if F_TABLE_SMART
        num_t nu = nu_min;
        num_t n = 1;
        while (nu <= nu_min && n < m) {
            nu_min = nu;
            ++n;

            const num_t a = f_table_calc(k-1, n-1);
            const num_t b = f_table_calc(k, m-n);
            nu = (a > b) ? a : b;
        }
#else /* !F_TABLE_SMART */
        for (num_t n = 2; n <= m; ++n) {
            const num_t a = f_table_calc(k-1, n-1);
            const num_t b = f_table_calc(k, m-n);
            const num_t nu = (a > b) ? a : b;
            if (nu < nu_min) {
                nu_min = nu;
            }
        }
#endif /* !F_TABLE_SMART */
        *fp = nu_min + 1;
    }

    return *fp;
}
