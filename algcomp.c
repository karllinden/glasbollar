/*
 * algcomp.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdlib.h>
#include <time.h>

#include <gop.h>

#include "f-table.h"
#include "fucalc.h"
#include "print-error.h"

#define K_DEFAULT 4
#define M_MAX_DEFAULT 1000
#define VALUES_DEFAULT 100

/* a simple stopwatch structure */
struct watch_s {
    struct timespec ts1;
    struct timespec ts2;
};
typedef struct watch_s watch_t;

static inline void __attribute__((nonnull))
watch_start(watch_t * const watch)
{
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &watch->ts1);
    return;
}

static inline void __attribute__((nonnull))
watch_stop(watch_t * const watch)
{
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &watch->ts2);
    return;
}

static inline long __attribute__((nonnull))
watch_get(const watch_t * const watch)
{
    return (watch->ts2.tv_sec - watch->ts1.tv_sec) * 1000000000 +
            watch->ts2.tv_nsec - watch->ts1.tv_nsec;
}

/* Returns a random integer in the mathematical range [0,range). */
static long int
randomrange(const long range)
{
    const long max = RAND_MAX - RAND_MAX % range;
    long r;
    do {
        r = random();
    } while (r > max);
    return r % range;
}

int __attribute__((nonnull))
main(int argc, char ** const argv)
{
    int exit_status = EXIT_SUCCESS;

    FILE * addfile = NULL;
    FILE * rawfile = NULL;

    char * addfilename = NULL;
    char * rawfilename = NULL;

    num_t k = K_DEFAULT;
    num_t m_max = M_MAX_DEFAULT;
    num_t values = VALUES_DEFAULT;

    int save = 0;

    const gop_option_t options[] = {
        {"addfile", '\0', GOP_STRING, &addfilename, NULL,
            "filename of add algorithm results", "FILENAME"},
        {"rawfile", '\0', GOP_STRING, &rawfilename, NULL,
            "filename of raw method results", "FILENAME"},
        {NULL, 'k', GOP_NUM_T, &k, NULL, "k-value", "K"},
        {"m-max", '\0', GOP_NUM_T, &m_max, NULL, "maximum m-value",
            "M_MAX"},
        {"save", 's', GOP_NONE, &save, NULL,
            "allow saving results between measurements", NULL},
        {"values", 'v', GOP_NUM_T, &values, NULL,
            "number of m-values to measure", "VALUES"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (addfilename == NULL) {
        print_error("you must specify --addfile");
        goto error;
    }
    if (rawfilename == NULL) {
        print_error("you must specify --rawfilename");
        goto error;
    }

    addfile = fopen(addfilename, "w");
    if (addfile == NULL) {
        print_error_errno("could not open %s for writing", addfilename);
        goto error;
    }

    rawfile = fopen(rawfilename, "w");
    if (rawfile == NULL) {
        print_error_errno("could not open %s for writing", rawfilename);
        goto error;
    }

    srandom((unsigned int)time(NULL));

    const num_t size = m_max / values;
    num_t extra = m_max % values;
    num_t begin = 0;
    while (begin < m_max) {
        num_t this_size = size;
        if (extra) {
            this_size++;
            extra--;
        }

        const num_t m = begin + (num_t)randomrange((long)this_size);

        if (!save) {
            f_table_free();
        }

        num_t r1;
        num_t r2;
        watch_t w1;
        watch_t w2;

        watch_start(&w1);
        r1 = fucalc(k, m);
        watch_stop(&w1);

        watch_start(&w2);
        r2 = f_table_calc(k, m);
        watch_stop(&w2);

        if (r1 != r2) {
            print_error("value mismatch for "
                        "(k,m)=(%"PRINUM",%"PRINUM"): got "
                        "%"PRINUM " and %"PRINUM, k, m, r1, r2);
            goto error;
        }

        fprintf(addfile, "%"PRINUM" %ld\n", m, watch_get(&w1));
        fprintf(rawfile, "%"PRINUM" %ld\n", m, watch_get(&w2));

        begin += this_size;
    }

    if (false) {
    error:
        exit_status = EXIT_FAILURE;
    }

    if (addfile != NULL && fclose(addfile)) {
        print_error_errno("could not close %s", addfilename);
    }
    if (rawfile != NULL && fclose(rawfile)) {
        print_error_errno("could not close %s", rawfilename);
    }

    return exit_status;
}
