/*
 * fplot.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

#include "f-table.h"
#include "print-error.h"

#define K_DEFAULT  2
#define M_MAX_DEFAULT 100

int __attribute__((nonnull))
main(int argc, char ** const argv)
{
    int exit_status = EXIT_SUCCESS;

    FILE * file = stdout;

    char * filename = NULL;

    num_t k = K_DEFAULT;
    num_t m_max = M_MAX_DEFAULT;

    const gop_option_t options[] = {
        {"output", 'o', GOP_STRING, &filename, NULL,
            "output filename", "FILENAME"},
        {NULL, 'k', GOP_NUM_T, &k, NULL, "k-value", "K"},
        {"m-max", '\0', GOP_NUM_T, &m_max, NULL, "max m-value",
            "M_MAX"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (f_table_alloc(k, m_max)) {
        goto error;
    }

    if (filename != NULL && filename[0] != '\0') {
        file = fopen(filename, "w");
        if (file == NULL) {
            print_error_errno("could not open %s for writing",
                              filename);
            goto error;
        }
    }

    for (num_t m = 0; m <= m_max; ++m) {
        fprintf(file, "%" PRINUM " %" PRINUM "\n", m,
                f_table_calc(k, m));
    }

    if (false) {
    error:
        exit_status = EXIT_FAILURE;
    }

    if (file != NULL && file != stdout) {
        if (fclose(file)) {
            print_error_errno("could not close file");
        }
    }

    return exit_status;
}
