/*
 * num.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdlib.h>

#include "num.h"

num_t __attribute__((const))
num_ceilsqrt(const num_t a)
{
    if (a <= 1) {
        return a;
    }

    num_t x = a >> 1;
    num_t y = x;
    do {
        x = y;
        y = (x + a/x) >> 1;
    } while (y < x);
    if (x*x < a) {
        ++x;
    }
    return x;
}
