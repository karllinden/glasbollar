set terminal epslatex
set output "fplot-3.tex"
set size .8
set style data lines
set key left top
set xlabel "$x$"
set ylabel "$y$
plot [x=0:100] "fplot-3.dat" title "$y = f(x)$"
