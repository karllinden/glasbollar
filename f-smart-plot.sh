#!/bin/sh
#
#  f-smart-plot.sh
#
#  Copyright (C) 2015 Karl Linden
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

script=`basename ${0}`

die() {
    echo "${script}: ${@}"
    exit 1
}

args=""
output=""

while test $# -gt 0
do
    case $1 in
        --output|-o) shift; output="$1";;
        *) args="${args} $1";;
    esac
    shift
done

file1=$(mktemp)
file2=$(mktemp)

./f-smart-plot --file1="${file1}" --file2="${file2}" ${args}

echo "set terminal epslatex
      set output \"${output}\"
      set size .8
      set style data lines
      set key left top
      set xlabel \"\$n\$\"
      plot \"${file1}\" title \"\$f_{k-1}(n-1)\$\", \
           \"${file2}\" title \"\$f_k(m-n)\$\"" \
        | gnuplot || die "gnuplot failed"

rm "${file1}" "${file2}" || die "rm failed"
