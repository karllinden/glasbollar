/*
 * fu-calc.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "fucalc.h"
#include "num.h"
#include "print-error.h"

static int
strtonum(const char * const str, num_t * const nump)
{
    char * endptr;
    unsigned long ul;

    errno = 0;
    ul = strtoul(str, &endptr, 0);
    if (ul > NUM_MAX) {
        errno = ERANGE;
    }
    if (errno != 0) {
        print_error_errno("could not convert argument");
        return 1;
    } else if (*endptr != '\0') {
        print_error("could not convert argument");
        return 1;
    }

    *nump = (num_t)ul;

    return 0;
}

int __attribute__((nonnull))
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;

    num_t k;
    num_t m;

    if (argc != 3) {
        puts("Calculates f_k(m) using the add algorithm");
        puts("Usage: fu-calc k m");
        goto error;
    }

    if (strtonum(argv[1], &k) || strtonum(argv[2], &m)) {
        goto error;
    }

    if (k == 0) {
        print_error("k may not be zero");
        goto error;
    }

    printf("%"PRINUM"\n", fucalc(k, m));

    exit_status = EXIT_SUCCESS;
error:
    return exit_status;
}
