/*
 * ftab.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

#include "f-table.h"
#include "print-error.h"

#define COLUMNS_DEFAULT 2
#define K_MAX_DEFAULT  3
#define M_MAX_DEFAULT 79

static char * caption = NULL;
static char * label = NULL;

static void __attribute__((nonnull))
write(FILE * const file,
      const unsigned int columns,
      const num_t m_max,
      const num_t k_max)
{
    num_t m_per_column = (m_max + 1) / columns;
    if (columns * m_per_column < m_max + 1) {
        m_per_column++;
    }

    fputs("\\begin{longtable}{", file);
    for (unsigned int c = 0; c < columns; ++c) {
        if (c > 0) {
            fputs("||", file);
        }
        fputs("r|", file);
        for (num_t k = 1; k < k_max; ++k) {
            fputc('r', file);
        }
    }
    fputs("}\n", file);

    if (caption != NULL) {
        fprintf(file, "\\caption[%s]{%s}\n", caption, caption);
    }
    if (label != NULL) {
        fprintf(file, "\\label{%s}\n", label);
    }

    fputs("\\\\\n", file);

    for (unsigned int c = 0; c < columns; ++c) {
        if (c > 0) {
            fputs("& ", file);
        }
        fputs("\\diagbox{$m$}{$k$} ", file);
        for (num_t k = 2; k <= k_max; ++k) {
            fprintf(file, "& %"PRINUM" ", k);
        }
    }
    fputs("\\\\\n", file);

    fputs("\\hline\n", file);

    for (num_t m_base = 0; m_base < m_per_column; ++m_base) {
        for (unsigned int c = 0; c < columns; ++c) {
            const num_t m = m_base + c * m_per_column;
            if (m <= m_max) {
                if (c > 0) {
                    fputs("& ", file);
                }
                fprintf(file, "%"PRINUM" ", m);
                for (num_t k = 2; k <= k_max; ++k) {
                    fprintf(file, "& %"PRINUM" ", f_table_calc(k, m));
                }
            }
        }
        fputs("\\\\\n", file);
    }

    fputs("\\end{longtable}\n", file);
 
    return;
}

int __attribute__((nonnull))
main(int argc, char ** const argv)
{
    int exit_status = EXIT_SUCCESS;

    FILE * file = stdout;

    char * filename = NULL;

    unsigned int columns = COLUMNS_DEFAULT;
    num_t k_max = K_MAX_DEFAULT;
    num_t m_max = M_MAX_DEFAULT;

    const gop_option_t options[] = {
        {"caption", '\0', GOP_STRING, &caption, NULL, "caption",
            "CAPTION"},
        {"columns", '\0', GOP_UNSIGNED_INT, &columns, NULL,
            "number of columns in the table", "COLUMNS"},
        {"filename", '\0', GOP_STRING, &filename, NULL,
            "output filename", "FILENAME"},
        {"k-max", '\0', GOP_NUM_T, &k_max, NULL, "max k-value",
            "K_MAX"},
        {"label", '\0', GOP_STRING, &label, NULL, "label", "LABEL"},
        {"m-max", '\0', GOP_NUM_T, &m_max, NULL, "max m-value",
            "M_MAX"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (f_table_alloc(k_max, m_max)) {
        goto error;
    }

    if (filename != NULL && filename[0] != '\0') {
        file = fopen(filename, "w");
        if (file == NULL) {
            print_error_errno("could not open %s for writing",
                              filename);
            goto error;
        }
    }

    write(file, columns, m_max, k_max);

    if (false) {
    error:
        exit_status = EXIT_FAILURE;
    }

    if (file != NULL && file != stdout) {
        if (fclose(file)) {
            print_error_errno("could not close file");
        }
    }

    return exit_status;
}
