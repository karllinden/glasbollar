/*
 * fucalc.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdlib.h>
#include <string.h>

#include "fucalc.h"
#include "num.h"

#define INDEX(i, j) ((i)&1)*(k-1)+(j-2)

num_t
fucalc(const num_t k, const num_t m)
{
    if (k == 1) {
        return m;
    } else if (k == 2) {
        return num_ceilsqrt(8*m + 1) >> 1;
    }

    num_t num[(k-1) << 2];
    memset(num, 0, (k-1) * sizeof(num_t));

    num_t i = 0;
    while (num[INDEX(i,k)] < m) {
        ++i;
        num[INDEX(i,2)] = num[INDEX(i-1,2)] + i;
        for (num_t j = 3; j <= k; ++j) {
            num[INDEX(i,j)] = num[INDEX(i-1,j-1)] + 
                              num[INDEX(i-1,j)]   + 1;

            /* overflow protection */
            if (num[INDEX(i,j)] <= num[INDEX(i-1,j)]) {
                /* since our num_t overflowed it must have gone beyond
                 * m, which means this is the answer. */
                return i;
            }
        }
    }

    return i;
}
