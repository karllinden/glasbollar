/*
 * smallnutab.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

#include "f-table.h"
#include "print-error.h"

#define K_DEFAULT 2
#define M_MAX_DEFAULT 20

static char * caption = NULL;
static char * label = NULL;

static void __attribute__((nonnull))
write_header(FILE * const file, const num_t m_max)
{
    fputs("\\diagbox{$m$}{$n$} &", file);
    for (num_t n = 1; n <= m_max; ++n) {
        fprintf(file, "%"PRINUM" & ", n);
    }
    fputs("\\\\\n"
          "\\toprule\n", file);
    return;
}

static void __attribute__((nonnull))
write_row(FILE * const file,
          const char * const name,
          const num_t m,
          const num_t m_max,
          const num_t min,
          const num_t row[])
{
    fputs(" & ", file);
    for (num_t n = 1; n <= m; ++n) {
        num_t this = row[n-1];
        if (this == min) {
            fprintf(file, "\\textbf{%" PRINUM "} & ", this);
        } else {
            fprintf(file, "%" PRINUM " & ", this);
        }
    }
    for (num_t n = m + 1; n <= m_max; ++n) {
        fputs("& ", file);
    }
    fputs(name, file);
    fputs(" \\\\\n", file);

    return;
}

static void __attribute__((nonnull))
write(FILE * const file,
      const num_t m_max,
      const num_t k)
{
    /* list of minimis for different m */
    num_t min[m_max];

    /* table of f and nu */
    num_t table[m_max][3][m_max];

    memset(min, 0xff, m_max * sizeof(num_t));

    for (num_t m = 1; m <= m_max; ++m) {
        for (num_t n = 1; n <= m; ++n) {
            const num_t a = f_table_calc(k-1, n-1);
            const num_t b = f_table_calc(k, m-n);
            const num_t nu = (a > b) ? a : b;
            table[m-1][0][n-1] = a;
            table[m-1][1][n-1] = b;
            table[m-1][2][n-1] = nu;
            if (min[m-1] > nu) {
                min[m-1] = nu;
            }
        }
    }

    fputs("\\begin{longtable}{c|", file);
    for (num_t m = 1; m <= m_max; ++m) {
        fputc('r', file);
    }
    fputs("|c}\n", file);

    if (caption != NULL) {
        fprintf(file, "\\caption[%s]{%s}\n", caption, caption);
    }
    if (label != NULL) {
        fprintf(file, "\\label{%s}\n", label);
    }

    fputs("\\\\\n", file);

    write_header(file, m_max);
    fprintf(file, "\\endfirsthead\n");
    write_header(file, m_max);
    fputs("\\endhead\n", file);

    for (num_t m = 1; m <= m_max; ++m) {
        write_row(file, "$f_{k-1}(n-1)$", m, m_max, min[m-1],
                  table[m-1][0]);
        fprintf(file, "%"PRINUM"", m);
        write_row(file, "$f_k(m-n)$", m, m_max, min[m-1],
                  table[m-1][1]);
        write_row(file, "$\\nu_k(m, n)$", m, m_max, min[m-1],
                  table[m-1][2]);
        fputs("\\bottomrule\n", file);
    }

    fputs("\\end{longtable}\n", file);
 
    return;
}

int __attribute__((nonnull))
main(int argc, char ** const argv)
{
    int exit_status = EXIT_SUCCESS;

    FILE * file = stdout;

    char * filename = NULL;

    num_t k = K_DEFAULT;
    num_t m_max = M_MAX_DEFAULT;

    const gop_option_t options[] = {
        {"caption", '\0', GOP_STRING, &caption, NULL, "caption",
            "CAPTION"},
        {"output", 'o', GOP_STRING, &filename, NULL, "output filename",
            "FILENAME"},
        {"label", '\0', GOP_STRING, &label, NULL, "label", "LABEL"},
        {NULL, 'k', GOP_NUM_T, &k, NULL, "k", "K"},
        {"m-max", '\0', GOP_NUM_T, &m_max, NULL, "maximum m",
            "M_MAX"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (k <= 1) {
        print_error("%"PRINUM" is not a valid argument to --k-min", k);
        goto error;
    }

    if (f_table_alloc(k, m_max)) {
        goto error;
    }

    if (filename != NULL && filename[0] != '\0') {
        file = fopen(filename, "w");
        if (file == NULL) {
            print_error_errno("could not open %s for writing",
                              filename);
            goto error;
        }
    }

    write(file, m_max, k);

    if (false) {
    error:
        exit_status = EXIT_FAILURE;
    }

    if (file != NULL && file != stdout) {
        if (fclose(file)) {
            print_error_errno("could not close file");
        }
    }

    return exit_status;
}
