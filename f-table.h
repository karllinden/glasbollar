/*
 * f-table.h
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef F_TABLE_H
# define F_TABLE_H

# include "num.h"

/* Call this to make f_table_alloc allocate much bigger chunks on demand
 * to reduce calls to malloc, memset and memcpy. */
void f_table_huge(void);

int f_table_alloc(const num_t k, const num_t m);
void f_table_free(void);
num_t f_table_calc(const num_t k, const num_t m);

#endif /* !F_TABLE_H */
