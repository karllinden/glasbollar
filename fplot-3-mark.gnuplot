set terminal epslatex
set output "fplot-3-mark.tex"
set size .8
set style data lines
set key left top
set xlabel "$x$"
set ylabel "$y$

unset xtics
set xtics format " "

i = 2
do for [x in "3 7 14 25 41 63 92"] {
    set arrow from x,0 to x,i nohead
    set xtics add (sprintf("$x_%d$",i) x)
    i = i + 1
}

plot [x=0:100] "fplot-3.dat" title "$y = f(x)$"
