set terminal epslatex
set output "fplot-2.tex"
set style data lines
set key spacing 1.6
set key bottom right
set xlabel "$m$"
plot \
    (sqrt(8*x+1)-1)/2 title "$\\frac{1}{2} (\\sqrt{8m+1}-1)$", \
    (sqrt(8*x+1)+1)/2 title "$\\frac{1}{2} (\\sqrt{8m+1}+1)$", \
    "fplot-2.dat" title "$f_2(m)$"
