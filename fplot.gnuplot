set terminal epslatex
set output "fplot.tex"
set style data lines
set key at 280,43
set xlabel "$m$"
set ylabel "$f_k(m)$
plot \
    "fplot-2.dat" title "$k=2$", \
    "fplot-3.dat" title "$k=3$", \
    "fplot-4.dat" title "$k=4$", \
    "fplot-5.dat" title "$k=5$"
