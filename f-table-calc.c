/*
 * f-table-calc.c
 * 
 * Copyright 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

#include "f-table.h"
#include "print-error.h"

int __attribute__((nonnull))
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;

    num_t k = 0;
    num_t m = 0;

    const gop_option_t options[] = {
        {NULL, 'k', GOP_NUM_T, &k, NULL, "k-value", "K"},
        {NULL, 'm', GOP_NUM_T, &m, NULL, "m-value", "M"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (k == 0 || m == 0) {
        print_error("specify -k and -m (to positive values)");
        goto error;
    }

    if (f_table_alloc(k, m)) {
        goto error;
    }

    printf("%"PRINUM"\n", f_table_calc(k, m));

    exit_status = EXIT_SUCCESS;
error:
    return exit_status;
}
